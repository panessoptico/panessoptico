
import * as THREE from 'three';
import { GUI } from 'three/addons/libs/lil-gui.module.min.js';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';

let camera, scene, renderer;

var defaultColor = { color : '#FFFFFF' };    

var totalVideos = [
    'acid_orange_wolrd',
    'ai_acid_persons_space',
    'ai_cyverpunk_alien',
    'ai_monkey_animals',
    'ai-80s-cyberpunk',
    'ai-animals-monster-octupus',
    'ai-city-space',
    'ai-crazy-queen',
    'ai-cyber-fractales',
    'ai-cyberpunk-space-1',
    'ai-faces-cyberpunk',
    'ai-minimal-ferrofluid',
    'ai-pscyho-art',
    'ai-space-station-retror', 
    'ai-spaces-planets-cyberpunk',
    'ai-star-fractales',
    'ai-system-ball',
    'ai-terror-eyes-1',
    'ai-terror-eyes-2',
    'ai-terror-space-2',
    'ai-terror-space',
    'ai-the-bad-touch',
    'flash-acid-black-white',
    'flash_personas_bailando_humo',
    'flash-espirales',
    'fractal_psychedelic',
    'fractal_world_1',
    'fractal_world_2',
    'fractal_world_3',
    'minimal_acid_1',
    'minimal_acid_2',
    'minimal_flash',
    'minimal_lines',
    'minimal-alsh-cube-lights',
    'minimal-lights-flash',
    'minimal-liquid-marine-gray',
    'minimal-screen-tv-5',
    'tunel_galaxia',
    'tunel-inferno',
    'ai-woman-fluids-acids',
    'ai-woman-dacing-front-2',
    'ai-woman-dacing-front',
    '-----------------',
    'acid_acuarela',
    'acid_icopor',
    'acid_orange_wolrd',
    'ai_acid_mosnters_wolrd',
    'ai_acid_persons_space',
    'ai_cyberpunk_monsters_dark',
    'ai_cyverpunk_alien_2',
    'ai_cyverpunk_alien',
    'ai-cyberpunk-space',
    'ai-cyberpunk-bosque',
    'ai-cyberpunk-planets-colors',
    'ai-cyberpunk-world',
    'ai-cyberpunk-world-ganesha',
    'ai_dancing_girls',
    'ai_fractal_wolrd_3',
    'ai_scare_face_virus',
    'ai_green_world_2',
    'ai_green_world_3',
    'ai_monkey_animals',
    'ai_scare_monsters',
    'ai_scare_montains_purple',
    'ai_scare_montains',
    'ai-world-afracnatomy',
    'ai-world-green-a',
    'ai-animals-monsters-1',
    'ai-animals-monsters-2',
    'ai-animals-monsters-3',
    'ai-animals-monsters-4',
    'ai-animals-monster-octupus',
    'ai-animal-octopus',
    'ai-animals-kitty',
    'ai-animals-gusano',
    'ai-animals-aveztrux',
    'ai-animals-white',
    'ai-animals-scare-marine-mosnters',
    'ai-animal-chimera',
    'ai-animals-evolution',
    'ai-animals-terror-an',
    'ai-animals-foca-truppy',
    'ai-b&w-scare-people',
    'ai-bio-world-green',
    'ai-world-kuthulu',
    'ai-city-cyberpunk-1',
    'ai-city-cyberpunk-2',
    'ai-city-cyberpunk-4', 
    'ai-city-cyberpunk-5',
    'ai-city-cyberpunk-8',
    'ai-city-cyberpunk-9',
    'ai-city-cyberpunk-robot',
    'ai-cyberpunk-world',
    'ai-cyberpunk-sicodelic-terror',
    'ai-cyberpunk-world-2',
    'ai-cyberpunk-world-3',
    'ai-city-cypberpunk-10',
    'ai-city-space',
    'ai-faces-cyberpunk',
    'ai-faces-big',
    'ai-faces-uglyes',
    'ai-faces-monjes',
    'ai-faces-robotics',
    'ai-faces-scare',
    'ai-faces-monjes',
    'ai-faces-uglyes',
    'ai-faces-scared-gray',
    'ai-faces-ladygaga',
    'ai-faces-ladygaga',
    'ai-faces-robot-evolution',
    'ai-forest-terror-gray',
    'ai-forest-dark',
    'ai-ganesha-purple',
    'ai-gray-scare-terror',
    'ai-gray-scare-terror-2',
    'ai-horror-chines-red',
    'ai-machines-2',
    'ai-machines',
    'ai-minimal-ferrofluid',
    'ai-picasso-art',
    'ai-pscyho-art',
    'ai-scare-mosnters-red',
    'ai-scare-people-zombie-dancing',
    'ai-scare-skulls',
    'ai-scare-skulls-2',
    'ai-scare-world',
    'ai-scarted-bio',
    'ai-scary-cartoon-faces', 
    'ai-scary-faces',
    'ai-sp-cities',
    'ai-sp-demons',
    'ai-sp-djs',
    'ai-sp-robot-faces-2',
    'ai-sp-robots-faces',
    'ai-sp-space-robots',
    'ai-sp-zombies',
    'ai-system-ball',
    'ai-terror-eyes-1',
    'ai-terror-eyes-2',
    'ai-terror-eyes-3',
    'ai-terror-forest-color',
    'ai-terror-forest-scary',
    'ai-terror-plants',
    'ai-terror-space-2',
    'ai-terror-space', 
    'ai-witches',
    'ai-wizards',
    'ai-woman-lips',
    'ai-woman-faces-3',
    'ai-woman-faces-5',
    'ai-woman-faces-6',
    'ai-woman-faces-7',
    'ai-world-afracnatomy',
    'ai-world-green-a',
    'ai-world-kuthulu',
    'ai-world-ornganic-machines',
    'ai-world-recorrido',
    'ai-zombies-cyborg',
    'eye_sphere',
    'flash_hands_rope',
    'flash_minimal_whiteheads',
    'flash_personas_bailando_humo',
    'flash_walking_alien',
    'flash-acid-black-white',
    'flash-espirales',
    'fluidos_acidos_1',
    'fractal_psychedelic',
    'fractal_world_1',
    'fractal_world_2',
    'fractal_world_3',
    'minimal_acid_1',
    'minimal_acid_2',
    'minimal_flash_3woman',
    'minimal_flash_angels_1',
    'minimal_flash_angels_2',
    'minimal-flash-scare-angels',
    'minimal_flash_black',
    'minimal_flash_dark_grayworld_1',
    'minimal_flash_dark_grayworld_2',
    'minimal_flash_dark_grayworld_3',
    'minimal_flash_fractal_lights',
    'minimal_flash_montana',
    'minimal_flash_scare_dark',
    'minimal_flash',
    'minimal_head_facemovements_1',
    'minimal_lines',
    'minimal-liquid-marine-gray',
    'minimal-squares',
    'minimal-screen-tv-2',
    'minimal-screen-tv-4',
    'minimal-screen-tv-5',
    'minimal-screen-tv-6',
    'minimal-screen-tv',
    'minimal-squares-beat',
    'minimla-screen-tv-3',
    'tune-real-neon',
    'tune-real-purlple',
    'tunel_galaxia_3',
    'tunel_galaxia_4',
    'tunel_galaxia',
    'tunel_neon_2',
    'tunel_neon_verde',
    'tunel_real_pink',
    'tunel_square_lights',
    'tunel_triangular',
    'tunel_triangulo_rojo',
    'tunel-arboles-digitales',
    'tunel-arboles-realistic',
    'tunel-cibernetico-2',
    'tunel-cibernetico-3',
    'tunel-cibernetico-6',
    'tunel-cibernetico-7',
    'tunel-cibernetico',
    'tunel-montanas',
    'tunel-nave-2',
    'tunel-raices-real',
    'tunel-square-b&w',
    'tunel_city_1',
    'tunel_city_2',
    'tunel_city_3',
    'tunel_city_4'

];

var params = {
    iframe1: 0,
    iframe2: 0,
    iframe3: 0,
    iframe4: 0,
    iframe5: 0,
    iframe6: 0,
    iframe7: 0,
    iframe8: 0,
    iframe9: 0,
	iframe10: 0,
	iframe11: 0,
	iframe12: 0,
	iframe13: 0,
    iframe14: 0,
    iframe15: 0,
    iframe16: 0,
    iframeParticle: 0,
    FLASH: 0,
    BACK: 0,
    MAIN: 0,
    videoBack:'fractal_psychedelic',
    videoBackMain:'fractal_psychedelic',
    videoflash:'on1',
    flashColor:'#FFFFFF',
    nameDj:'CALLE 9',
    timeLine:0.0,
    setTimeLine:false,
};

 
var videos1 = [
    'ai-minimal-ferrofluid',
    'ai-picasso-art',
    'acid_orange_wolrd',
    'ai-cyber-fractales',
    'ai-smoking-colors-2',
    'ai-smoking-colors-4',
    'ai-smoking-colors-3',
    'ai-smoking-colors-5',
    'ai-smoking-colors',
    'ai-acid-ranas-hongos',
    'ai-mandalas',
    'ai-cyberpunk-city-13',
    'ai-cyberpunk-city-15', 
    'ai-pscyho-art', 
    'ai-cyberpunk-world-3',
    'ai-cyberpunk-trees', 
   
]
 
var videos2 = [  
    'ai-city-cypberpunk-10',
    'ai-retro-robots',
    'ai-mosnters-city',
    'ai-city-cyberpunk-2',
    'ai-cyberpunk-planets-colors',
    'ai-scared-mushrooms',
    'ai-city-space',
    'ai-cyberpunk-space',
    'ai-lines-fractales',
]

var videos3 = [  
    'ai-the-jin',
    'ai-future-terror-b&w',
    'ai-bio-mech-parts',
    'ai_scare_montains_purple',
    'ai-cyber-shiva',
    'ai-faces-mate-color',
    'ai-green-space-cells',
    'salvador-dali-anim',
    'ai_acid_persons_space',
    'ai-cyberpunk-faces-space',
    'ai-cyberpunk-sicodelic-terror',
    'ai-cyber-robots-1',
    'ai-cyberpunk-world-ganesha',
    'ai-faces-monjes',
    'ai-faces-robot-evolution',
    'ai-lab',
    'ai-long-woman-robotics',
    'ai-retro-lines',
    'ai-terror-space-2',
    'ai-wizards',
]
 
var videos4 = [ 
    'ai-animals-monsters-2',
    'ai-animals-monsters-3',
    'ai-future-casteles',
    'ai-animals-monster-octupus',
    'ai-animal-octopus',
    'ai-animal-woman',
    'ai-ganesha-purple',
    'ai-terror-plants',
    'ai-terror-mosnters-gray',
    'ai-terror-forest-scary',
    'ai-terror-forest-color',
]

 
var videos5 = [
    'ai-fying-marine-astronaut',
    'tunel_galaxia_3',
    'tune-real-purlple',
    'tunel_real_pink',
    'tunel_triangulo_rojo',
    'tunel_square_lights',
    'tunel-80s-retro-1',
    'tunel-80s-retro-6',
    'tunel-80s-retro-4',
    'tunel-80s-retro-3',
    'tunel_city_2',
    'tunel-80s-retro-7',
    'tunel-cibernetico-2',
    'tunel-cibernetico-7',
    'tunel-80s-retro-5',
    'tunel_city_3', 
    'tunel_city_1',
    'tunel-nave-2',
    'tunel-rayas',
]


 
var videos6 = [
    'ai-anime-monster-white',
    'ai-cyberpuk-spaces-human-muscles',
    'ai-woman-pass-mediaval',
    'ai-crazy-queen',
    'ai-cyber-knights',
    'ai-scary-cartoon-faces',
    'ai-scare-robots-cables',
    'ai-scary-faces',
    'ai-alice-wonderlan',
    'ai-scare-marine-faces',
    'ai-scare-skulls-job',
]

 
 
var videos7 = [
    'ai-faces-big',
    'ai-faces-scared-gray',
    'ai-woman-vampire',
    'ai-cyber-terro-pink-indside',
    'ai-scare-ninfa-purple',
    'ai-city-caos-zombies',
    'ai-cyberpunk-bar-womas',
    'ai-scare-scullls-mussrooms-2',
    'ai-eyes-terror-6',
    'ai-terror-woman-walker',
    'ai-woman-robot-gray',
    'ai-zombies-land',
    'ai-demons-bodies',
    'ai-forest-terror-gray',
    'ai-fractales-cyberpunk-terror',
    'ai-purples-dark-woman',
    'ai-scared-faces-red-eyes',
    'ai-terror-eyes-1',
    'ai-witches',
    'ai-terror-eyes-2',
]



 
var videos8 = [ 
    'ai-dragon-transition',
    'ai-alice-bad-trip',
    'ai-scare-skulls',
    'ai-cyber-tarantula',
    'ai_cyberpunk_monsters_dark',
    'ai-rare-animals-ccyber-2',
    'ai-animales-scare',
    'ai-eyes-terror-20',
    'ai-scare-wolf-red',
    'ai-eyes-terror-19',
    'ai-faces-cyberpunk',
    'ai-rare-animals-ccyber',
    'ai-terror-forest',
    'ai-eyes-terror-18',
    'ai-eyes-terror-11',
]

 
var videos9 = [
    'ai-eyes-terror-10',
    'ai-cybrpunk-body-parts',
    'ai-cyber-rob-termin',
    'ai-terror-faces-capas',
    'ai-eyes-terror-4',
    'ai-eyes-terror-5',
    'ai-eyes-terror-17',
    'ai-punkeros-cyberpunk',
    'ai-eyes-terror-7',
    'ai-eyes-terror-8',
    'ai-eyes-terror-14',
    'ai-terror-faces-mushrooms',
    'ai-aliens-faces-purples',
    'ai-eyes-terror-15',
    'ai-terror-faces-front',
]

 

var videos10 = [
    'ai-red-dark-terror',
    'ai-evil-clown-1',
    'terror-garden-3',
    'ai-eyes-terror-9',
    'ai-evil-clown-2',
    'terror-garden-2',
    'ai-sp-demons',
    'ai-eyes-terror-12',
    'terror-garden-1',
    'ai-evil-clown-3',
    'ai-eyes-terror-13',
    'ai-terror-back-walkig',
    'ai-eyes-terror-16',
    'ai-scare-doctor-monster',
    'ai-eyes-terror-21',
]



var videos11 = [
    'ai-splash-gray-skull',
    'ai-music-consoles',
    'ai-b&w-scare-people',
    'ai-bailarina-acuarela',
    'ai-scare-ghosts',
    'ai-scare-people-zombie-dancing',
]



 
 

var videosParticles = [
    'weed-history',
    'wizkhalifa-psycho',
    'sp-adult-swim',
    'ai-monkey-robot',
    'ai-roses-garden',
    'ai-woman-dacing-water',
    'ai-woman-dacing-front-2',
    'ai-woman-dacing-front',
    'ai_scare_face_virus',
    'ai-scare-people-zombie-dancing',
    'ai-zombie-dancing-doble',
    'minimal-lights-flash',
    'minimal-screen-tv-2',
    'minimal-screen-tv-4',
    'minimal-screen-tv-5',
    'minimal-screen-tv-6',
    'minimal-screen-tv',
    'minimal-squares-beat',
    'minimla-screen-tv-3',
    'minimal_acid_1',
    'minimal_acid_2',
    'minimal_flash_3woman',
    'minimal_flash_angels_1',
    'minimal_flash_angels_2',
    'minimal-flash-scare-angels',
    'minimal_flash_black',
    'minimal_flash_dark_grayworld_1',
    'minimal_flash_dark_grayworld_2',
    'minimal_flash_dark_grayworld_3',
    'minimal_flash_fractal_lights',
    'minimal_flash_montana',
    'minimal_flash_scare_dark',
    'minimal_flash',
    'minimal_head_facemovements_1',
    'minimal_lines',
    'minimal-liquid-marine-gray',
    'minimal-squares',
    'loop-fluidos-neon',
    'head-malla',
    'head_skull_rotating',
    'head_facemovements',
    'fractal_world_2',
    'fractal_world_1',
    'fractal_psychedelic',
    'fluidos_acidos_1',
    'flash-espirales',
    'flash_minimal_whiteheads',
    'flash_cyberpunk_skull',
    'ai-scary-cartoon-faces',
    'dark_david_skull',
    'dark_floating_skulls',
    'minimal-squares',
    'minimal_lines',
    'flash_minimal',
    'ai-pscyho-art',
    'ai-minimal-ferrofluid',
    'flash_minimal_scare_dark',
    'flash_minimal_black',
    
]

init();
render();

var myvideo = document.getElementById('background-video'); 
var currentTime = 0.2;

myvideo.addEventListener("timeupdate", (event) => {
    if (currentTime === myvideo.currentTime ){
        console.log("Click");
        params.timeLine = myvideo.currentTime;
        params.setTimeLine = true; 
        updateParams();
    }

    setTimeout(() => {
        params.setTimeLine = false; 
        updateParams();
    }, 500);

    currentTime = myvideo.currentTime; 
});

 

function init() {

   
    const container = document.createElement('div');
    document.body.appendChild(container);

    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);


    camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 2000 );
    camera.position.set( 0, 100, 0 );

   
    scene = new THREE.Scene();
    const controls = new OrbitControls( camera, renderer.domElement );
    controls.addEventListener( 'change', render ); // use if there is no animation loop
    controls.minDistance = 400;
    controls.maxDistance = 1000;
    controls.target.set( 10, 90, - 16 );
    controls.update();

    window.addEventListener( 'resize', onWindowResize );

    initGUI();


    document.onkeypress = function (e) {
        e = e || window.event; 
        startFlash(e.keyCode);
    };

}

function startFlash(code){

  console.log("code: "+code);

    var activate = false;
   let number =  Number(code);
   if(number == 113 || number == 81){
    params.videoflash = 'on1';
    activate = true;
   }else if(number == 119|| number == 87){
    params.videoflash = 'on2';
    activate = true;
   }else if(number == 101 || number == 69){
    params.videoflash = 'on3';
    activate = true;
   }else if(number == 114 || number == 82){
    params.videoflash = 'on4';
    activate = true;
   }else if(number == 116 || number == 84){
    params.videoflash = 'on5';
    activate = true;
   }else if(number == 121 || number == 89){
    params.videoflash = 'on6';
    activate = true;
   }else if(number == 115 || number == 83){
    params.videoflash = 'off';
    params.FLASH = '0.0';
    activate = false;
    updateParams();
   }

   if(activate == true){
    params.FLASH = '1';
    //params.BACK = '0.40';
    updateParams();
   }
   
}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

    render();

}

//



function initGUI() {

    const gui = new GUI();

    const djName = gui.addFolder('NOMBRE DJ');
    djName.add(params, "nameDj").onFinishChange(function (value) {
        updateParams();
    });

 
    const videosFolderBack = gui.addFolder('VIDEO BACKGROUND');
    videosFolderBack.add(params, 'BACK', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    videosFolderBack.add({ level: totalVideos[0] }, 'VIDEO BACK', totalVideos ).onChange(function(value){
        changeVideoBackgroundBack(value);
    });
    videosFolderBack.open();


    const videosFolder = gui.addFolder('VIDEO MAIN');
    videosFolder.add(params, 'MAIN', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    videosFolder.open();


    const flashFolder = gui.addFolder('FLASH')
    flashFolder.addColor(defaultColor, 'color').onChange( function(colorValue) {
        changeFlashColor(colorValue);
    });

    flashFolder.open();

 
    const lineart = gui.addFolder('-----------------------------------------');
    lineart.close();


    const frame1 = gui.addFolder('IFRAME-1')

    frame1.add(params, 'iframe1', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    frame1.add({ level: videos1[0] }, 'video fr1', videos1 ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame1.open()


    const frame2 = gui.addFolder('IFRAME-2')
    frame2.add(params, 'iframe2', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    frame2.add({ level: videos2[0] }, 'video fr2', videos2 ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame2.close()



    const frame3 = gui.addFolder('IFRAME-3')
    frame3.add(params, 'iframe3', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    frame3.add({ level: videos3[0] }, 'video fr3', videos3 ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame3.close()



    const frame4 = gui.addFolder('IFRAME-4')
    frame4.add(params, 'iframe4', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    frame4.add({ level: videos4[0] }, 'video fr4', videos4 ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame4.close()




    const frame5 = gui.addFolder('IFRAME-5')
    frame5.add(params, 'iframe5', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    frame5.add({ level: videos5[0] }, 'video fr5', videos5 ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame5.close()




    const frame6 = gui.addFolder('IFRAME-6')
    frame6.add(params, 'iframe6', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    frame6.add({ level: videos6[0] }, 'video fr6', videos6 ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame6.close()




    const frame7 = gui.addFolder('IFRAME-7')
    frame7.add(params, 'iframe7', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    frame7.add({ level: videos7[0] }, 'video fr7', videos7 ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame7.close()




    const frame8 = gui.addFolder('IFRAME-8')

    frame8.add(params, 'iframe8', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });

    frame8.add({ level: videos8[0] }, 'video fr8', videos8 ).onChange(function(value){
        changeVideoBackground(value);
    });

    frame8.close()



    const frame9 = gui.addFolder('IFRAME-9')

    frame9.add(params, 'iframe9', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });

    frame9.add({ level: videos9[0] }, 'video fr9', videos9 ).onChange(function(value){
        changeVideoBackground(value);
    });

    frame9.close()   


    const frame10 = gui.addFolder('IFRAME-10')

    frame10.add(params, 'iframe10', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });

    frame10.add({ level: videos10[0] }, 'video fr10', videos10 ).onChange(function(value){
        changeVideoBackground(value);
    });

    frame10.close()   


    const frame11 = gui.addFolder('IFRAME-11')

    frame11.add(params, 'iframe11', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });

    frame11.add({ level: videos11[0] }, 'video fr11', videos11 ).onChange(function(value){
        changeVideoBackground(value);
    });

    frame11.close()  

 /*
    const frame12 = gui.addFolder('IFRAME-12')

    frame12.add(params, 'iframe12', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });

    frame12.add({ level: videos12[0] }, 'video fr12', videos12 ).onChange(function(value){
        changeVideoBackground(value);
    });

    frame12.close()  


    const frame13 = gui.addFolder('IFRAME-13')

    frame13.add(params, 'iframe13', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });

    frame13.add({ level: videos13[0] }, 'video fr13', videos13 ).onChange(function(value){
        changeVideoBackground(value);
    });

    frame13.close()  

 */

    const frameParticle = gui.addFolder('IFRAME-PARTICLES')

    frameParticle.add(params, 'iframeParticle', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });

    frameParticle.add({ level: videosParticles[0] }, 'video frame particle', videosParticles ).onChange(function(value){
        changeVideoBackground(value);
    });
    
    frameParticle.open()




    const guiTwo = new GUI();

    const djNameTwo = guiTwo.addFolder('NOMBRE DJ');
    djNameTwo.add(params, "nameDj").onFinishChange(function (value) {
        updateParams();
    });

    
}

function updateParams(){ 
    let json = JSON.stringify(params); 
    localStorage.setItem('INTERACTIVEVISUALS',json );
}

function changeFlashColor(color){
    params.flashColor = color;
    updateParams();
}
 

function changeVideoBackgroundBack(videoName){

    var video = document.getElementById('background-video-secondary');
    var src = '../../VIDEO_VISUALS/'+videoName+'.mp4';
    video.setAttribute("src", src);

    params.videoBack= videoName;
    updateParams();
}

function changeVideoBackground(videoName){

    var video = document.getElementById('background-video');
    var src = '../../VIDEO_VISUALS/'+videoName+'.mp4';
    video.setAttribute("src", src);

    params.videoBackMain = videoName;
    updateParams();
}

function changeVideoBackground2(videoName){
    params.videoflash = videoName;
    updateParams();
}

function render() { 
    renderer.render(scene, camera);
}