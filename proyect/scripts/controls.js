
import * as THREE from 'three';
import { GUI } from 'three/addons/libs/lil-gui.module.min.js';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';

let camera, scene, renderer;

var defaultColor = { color : '#FFFFFF' };   

var params = {
    iframe1: 0,
    iframe2: 0,
    iframe3: 0,
    iframe4: 0,
    iframe5: 0,
    iframe6: 0,
    iframe7: 0,
    iframe8: 0,
    iframe9: 0,
	iframe10: 0,
	iframe11: 0,
	iframe12: 0,
	iframe13: 0,
    iframe14: 0,
    iframe15: 0,
    iframe16: 0,
    iframe17: 0,
    iframe18: 0,
    iframeParticle: 0,
    iframeCameraFilter: 0,
    iframeJeelizHelmet: 0,
    FLASH: 0,
    BACK: 0,
    MAIN: 0,
    videoBack:'fractal_psychedelic',
    videoBackMain:'fractal_psychedelic',
    videoflash:'on1',
    flashColor:'#FFFFFF',
    nameDj:'CALLE 9',
    timeLine:0.0,
    setTimeLine:false,
    syncVideos:false,
};

var launchedVideos = new Array();

init();
render();

var btnSyncVideos = document.getElementById('btnSyncVideos'); 
btnSyncVideos.addEventListener("click", syncVideos);

var btnLaunchMainVideo = document.getElementById('btnLaunchMainVideo'); 
btnLaunchMainVideo.addEventListener("click", launchMainVideo);


var btnLaunchBackVideo = document.getElementById('btnLaunchBackVideo'); 
btnLaunchBackVideo.addEventListener("click", launchBackVideo);

var myvideo = document.getElementById('background-video'); 
var currentTime = 0.2;
var selectedVideoName = "fractal_psychedelic"
var selectedVideoBackName = "fractal_psychedelic"

myvideo.addEventListener("timeupdate", (event) => {
    if (currentTime === myvideo.currentTime ){
        console.log("Click");
        params.timeLine = myvideo.currentTime;
        params.setTimeLine = true; 
        updateParams();
    }

    setTimeout(() => {
        params.setTimeLine = false; 
        updateParams();
    }, 500);

    currentTime = myvideo.currentTime; 
});

 

function init() {

    launchedVideos = new Array();
   
    const container = document.createElement('div');
    document.body.appendChild(container);

    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);


    camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 2000 );
    camera.position.set( 0, 100, 0 );

   
    scene = new THREE.Scene();
    const controls = new OrbitControls( camera, renderer.domElement );
    controls.addEventListener( 'change', render ); // use if there is no animation loop
    controls.minDistance = 400;
    controls.maxDistance = 1000;
    controls.target.set( 10, 90, - 16 );
    controls.update();

    window.addEventListener( 'resize', onWindowResize );

    initGUI();


    document.onkeypress = function (e) {
        e = e || window.event; 
        startFlash(e.keyCode);
    };

}

function startFlash(code){

  console.log("code: "+code);

    var activate = false;
   let number =  Number(code);
   if(number == 113 || number == 81){
    params.videoflash = 'on1';
    activate = true;
   }else if(number == 119|| number == 87){
    params.videoflash = 'on2';
    activate = true;
   }else if(number == 101 || number == 69){
    params.videoflash = 'on3';
    activate = true;
   }else if(number == 114 || number == 82){
    params.videoflash = 'on4';
    activate = true;
   }else if(number == 116 || number == 84){
    params.videoflash = 'on5';
    activate = true;
   }else if(number == 121 || number == 89){
    params.videoflash = 'on6';
    activate = true;
   }else if(number == 115 || number == 83){
    params.videoflash = 'off';
    params.FLASH = '0.0';
    activate = false;
    updateParams();
   }

   if(activate == true){
    params.FLASH = '1';
    //params.BACK = '0.40';
    updateParams();
   }
   
}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

    render();

}

//



function initGUI() {

    const gui = new GUI();

    const djName = gui.addFolder('NOMBRE DJ');
    djName.add(params, "nameDj").onFinishChange(function (value) {
        updateParams();
    });

 
 




    const flashFolder = gui.addFolder('FLASH')
    flashFolder.addColor(defaultColor, 'color').onChange( function(colorValue) {
        changeFlashColor(colorValue);
    });

    flashFolder.open();

 
    const lineart = gui.addFolder('-----------------------------------------');
    lineart.close();

    for (let index = 0; index < OBJECTS3D.length; index++) {
        let frameLabel = (index + 1)
        let object = OBJECTS3D[index];

        console.log("object: "+JSON.stringify(object));
        console.log("frameLabel: "+frameLabel);
        console.log("object.name: "+object.name);
        
        const frame = gui.addFolder('IFRAME-'+frameLabel+'-'+object.name)
        frame.add(params, 'iframe'+frameLabel, 0, 1).step(0.01).onChange(function (value) {
            updateParams();
        });
        frame.close()
        
    }


    const frameParticle = gui.addFolder('IFRAME-PARTICLES')
    frameParticle.add(params, 'iframeParticle', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    frameParticle.close()

    const frameCameraFilter = gui.addFolder('IFRAME-CAMERA-FILTER')
    frameCameraFilter.add(params, 'iframeCameraFilter', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    frameCameraFilter.close()

   /* const frameJeelizHelmet = gui.addFolder('IFRAME-JEELIZ-HELMET')
    frameJeelizHelmet.add(params, 'iframeJeelizHelmet', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    frameJeelizHelmet.open()*/

    

 

    const guiBackVideo = new GUI({ autoPlace: false, "width": "100%" });

    const videosFolderBack = guiBackVideo.addFolder('OPACITY VIDEO BACKGROUND      ');
    videosFolderBack.add(params, 'BACK', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    videosFolderBack.open();

    var customContainer = document.getElementById('videoBackOpacitySlider');
    customContainer.appendChild(guiBackVideo.domElement);



    const guiMainVideo = new GUI({ autoPlace: false, "width": "100%" });
    const videosFolderMain = guiMainVideo.addFolder('OPACITY VIDEO MAIN');
    videosFolderMain.add(params, 'MAIN', 0, 1).step(0.01).onChange(function (value) {
        updateParams();
    });
    videosFolderMain.open();
    var customContainer = document.getElementById('videoMainOpacitySlider');
    customContainer.appendChild(guiMainVideo.domElement);





  initGUIVideos();

    
}

function initGUIVideos(){ 


    const gui = new GUI({ autoPlace: false });

/*
    const lineartb = gui.addFolder('-----------BACKGROUND------------');
    lineartb.close();

    const videosFolderBack = gui.addFolder('VIDEO BACKGROUND');
    videosFolderBack.add({ level: VisualVideos.background[0] }, 'VIDEO BACK', VisualVideos.background ).onChange(function(value){
        changeVideoBackgroundBack(value);
    });
    videosFolderBack.open();
*/
   

    const lineart = gui.addFolder('-------------VIDEOS----------------');
    lineart.close();


    const frame6 = gui.addFolder('AI-FRACTALES')
    frame6.add({ level: VisualVideos.fractals[0] }, 'AI-FRACTALES', VisualVideos.fractals ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame6.close()
 


    const frame12 = gui.addFolder('AI-CITIES')
    frame12.add({ level: VisualVideos.cities[0] }, 'AI-CITIES', VisualVideos.cities ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame12.close()
 

    const frame3 = gui.addFolder('AI-WORLDS-MONSTERS')
    frame3.add({ level: VisualVideos.worldMonsters[0] }, 'AI-WORLDS-MONSTERS', VisualVideos.worldMonsters ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame3.close()
 


    const frame1 = gui.addFolder('VIDEO 80S')
    frame1.add({ level: VisualVideos.ochentas[0] }, 'VIDEO 80S', VisualVideos.ochentas ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame1.close()
 

    const frame2 = gui.addFolder('ACID')
    frame2.add({ level: VisualVideos.acid[0] }, 'ACID', VisualVideos.acid ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame2.close()
 


    const frame4 = gui.addFolder('AI-SCARE-BODIES')
    frame4.add({ level: VisualVideos.scarieBodies[0] }, 'AI-SCARE-BODIES', VisualVideos.scarieBodies ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame4.close()
 


    const frame5 = gui.addFolder('AI-WOMANS')
    frame5.add({ level: VisualVideos.womans[0] }, 'AI-WOMANS', VisualVideos.womans ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame5.close()
   


    const frame7 = gui.addFolder('AI-ANIMALS')
    frame7.add({ level: VisualVideos.animals[0] }, 'AI-ANIMALS', VisualVideos.animals ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame7.close()
 

    const frame8 = gui.addFolder('AI-ENGRANAJES')
    frame8.add({ level: VisualVideos.engranajes[0] }, 'AI-ENGRANAJES', VisualVideos.engranajes ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame8.close()
  

    const frame9 = gui.addFolder('AI-SYCODELIC')
    frame9.add({ level: VisualVideos.psychodelic[0] }, 'AI-SYCODELIC', VisualVideos.psychodelic ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame9.close()
  

    const frame10 = gui.addFolder('AI-TERROR')
    frame10.add({ level: VisualVideos.terror[0] }, 'AI-TERROR', VisualVideos.terror ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame10.close()
 

    const frame11 = gui.addFolder('AI-ANIME')
    frame11.add({ level: VisualVideos.anime[0] }, 'AI-ANIME', VisualVideos.anime ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame11.close()
 



    const frame13 = gui.addFolder('AI-ROBOTS')
    frame13.add({ level: VisualVideos.robots[0] }, 'AI-ROBOTS', VisualVideos.robots ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame13.close()
  


    const frame14 = gui.addFolder('AI-REGUETON')
    frame14.add({ level: VisualVideos.regueton[0] }, 'AI-REGUETON', VisualVideos.regueton ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame14.close()
    







    const frame97 = gui.addFolder('TUNELES')
    frame97.add({ level: VisualVideos.tunel[0] }, 'TUNELES', VisualVideos.tunel ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame97.close()




    const frame98 = gui.addFolder('MINIMAL')
    frame98.add({ level: VisualVideos.minimal[0] }, 'MINIMAL', VisualVideos.minimal ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame98.close()

    const frame999 = gui.addFolder('STORIES')
    frame999.add({ level: VisualVideos.stories[0] }, 'STORIES', VisualVideos.stories ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame999.close()
    

    const frame9999 = gui.addFolder('ANAGLYPH')
    frame9999.add({ level: VisualVideos.anaglyph[0] }, 'ANAGLYPH', VisualVideos.anaglyph ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame9999.close()

    const frame999999999 = gui.addFolder('ARVAK')
    frame999999999.add({ level: VisualVideos.arvak[0] }, 'ARVAK', VisualVideos.arvak ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame999999999.close()
    


    const frame99 = gui.addFolder('TOP-VIDEOS')
    frame99.add({ level: VisualVideos.top[0] }, 'TOP-VIDEOS', VisualVideos.top ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame99.close()


    const frame99Larges = gui.addFolder('LARGES-VIDEOS')
    frame99Larges.add({ level: VisualVideos.larges[0] }, 'LARGES-VIDEOS', VisualVideos.larges ).onChange(function(value){
        changeVideoBackground(value);
    });
    frame99Larges.close()


    var customContainer = document.getElementById('videGuiContainer');
    customContainer.appendChild(gui.domElement);

    

}

function updateParams(){ 
    let json = JSON.stringify(params); 
    localStorage.setItem('INTERACTIVEVISUALS',json );
}

function changeFlashColor(color){
    params.flashColor = color;
    updateParams();
}
 
 
function changeVideoBackground(videoName){


    selectedVideoName = videoName;
    var video = document.getElementById('background-video-preview');
    //var videoDuration = document.getElementById('launchVideoTime');
    var src = '../../VIDEO_VISUALS/'+videoName+'.mp4';
    video.setAttribute("src", src); 
   // var response = fancyTimeFormat(video.duration);
   // videoDuration.innerHTML = response;


    const found = launchedVideos.find(element => element === videoName );
    console.log("found: "+found);
    var warningMessage = document.getElementById('warningMessage');
    if(found != undefined){
        warningMessage.style.display = "block";
    } else {
        warningMessage.style.display = "none";
    }

    
}


function fancyTimeFormat(duration) {
    console.log("-----"+duration);
    var m = parseInt(duration / 60, 10);
    var s = duration % 60;
    return m + ":" + s;
  }


  function changeVideoBackgroundBack(videoName){

    var video = document.getElementById('background-video-secondary');
    var src = '../../VIDEO_VISUALS/'+videoName+'.mp4';
    video.setAttribute("src", src);

    params.videoBack= videoName;
    updateParams();
}

function launchBackVideo(){ 
    var video = document.getElementById('background-video-secondary');
    var src = '../../VIDEO_VISUALS/'+selectedVideoName+'.mp4';
    video.setAttribute("src", src);

    launchedVideos.push(selectedVideoName);

    params.videoBack = selectedVideoName;
    updateParams();
}

function syncVideos(){ 
    console.log("controls syncVideos ");
    params.syncVideos = true;
    updateParams();

    setTimeout(() => {
        params.syncVideos = false;
        updateParams();
    }, 300);
    
}

function launchMainVideo(){ 
    var video = document.getElementById('background-video');
    var src = '../../VIDEO_VISUALS/'+selectedVideoName+'.mp4';
    video.setAttribute("src", src);

    launchedVideos.push(selectedVideoName);

    params.videoBackMain = selectedVideoName;
    updateParams();
}

function changeVideoBackground2(videoName){
    params.videoflash = videoName;
    updateParams();
}



function render() { 
    renderer.render(scene, camera);
}