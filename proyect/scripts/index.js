
import * as THREE from 'three';

import { RoomEnvironment } from 'three/addons/environments/RoomEnvironment.js';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';

import { KTX2Loader } from 'three/addons/loaders/KTX2Loader.js';
import { MeshoptDecoder } from 'three/addons/libs/meshopt_decoder.module.js';

let camera, scene, renderer;

var params = {
    iframe1: 0,
    iframe2: 0,
    iframe3: 0,
    iframe4: 0,
    iframe5: 0,
    iframe6: 0,
    iframe7: 0,
    iframe8: 0,
    iframe9: 0,
	iframe10: 0,
	iframe11: 0,
	iframe12: 0,
	iframe13: 0,
	iframe14: 0,
	iframe15: 0,
	iframe16: 0,
	iframe17: 0,
    iframe18: 0,
	iframeParticle: 0,
	iframeCameraFilter: 0,
	iframeJeelizHelmet: 0,
    FLASH: 0,
    BACK: 0,
    MAIN: 0,
    videoBack:'fractal_psychedelic',
    videoBackMain:'fractal_psychedelic',
    videoflash:'on1',
	flashColor:'#FFFFFF',
	nameDj:'CALLE 9',
	timeLine:0.0,
    setTimeLine:false,
	syncVideos:false,
};



var currentFlashcolor = '#FFFFFF';
var currentVideo = 'fractal_psychedelic';
var currentVideoMain = 'fractal_psychedelic';
var currentVideo2 = 'flash_black_white_faster';

init();

function init() {

	const container = document.createElement('div');
	document.body.appendChild(container);

	renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.toneMapping = THREE.ACESFilmicToneMapping;
	renderer.toneMappingExposure = 1;
	renderer.outputEncoding = THREE.sRGBEncoding;
	container.appendChild(renderer.domElement);

	camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000);
	camera.position.set(0, 100, 0);

	const environment = new RoomEnvironment();
	const pmremGenerator = new THREE.PMREMGenerator(renderer);

	scene = new THREE.Scene();
	//scene.background = new THREE.Color( 0xbbbbbb );
	scene.environment = pmremGenerator.fromScene(environment).texture;
	environment.dispose();

	document.onkeypress = function (e) {
        e = e || window.event; 
        startFlash(e.keyCode);
    };

	set3DObjects();

	window.addEventListener('resize', onWindowResize);

	setTimeout(() => { 
		render();
	  }, "10000")

 

}

function set3DObjects(){
	var counter = 1;
	OBJECTS3D.forEach(obj => {
		let str = 	' <div id="iframe'+counter+'" '+
				  	' class="sketchfab-embed-wrapper"> '+
					' <iframe '+
					' class="sketchfab-embed-wrapper-iframe" '+
					' frameborder="0" '+
					' allowfullscreen '+
					' mozallowfullscreen="true" '+
					' webkitallowfullscreen="true" '+
					' allow="autoplay; fullscreen; xr-spatial-tracking" '+
					' xr-spatial-tracking '+
					' execution-while-out-of-viewport '+
					' execution-while-not-rendered '+
					' web-share '+
					' src="https://sketchfab.com/models/'+obj.id+'/embed?autospin='+obj.spin+'&autostart=1&preload=1&transparent=1" >'+
					' </iframe> '+
					' </div> ';
		document.getElementById('frameContainer').innerHTML += str;
		counter++;
	});


	let particlesFrame = '<div id="iframeParticle" '+
				  	' class="sketchfab-embed-wrapper"> '+
					' <iframe '+
					' class="sketchfab-embed-wrapper-iframe" '+
					' frameborder="0" '+
					' allowfullscreen '+
					' mozallowfullscreen="true" '+
					' webkitallowfullscreen="true" '+
					' allow="autoplay; fullscreen; xr-spatial-tracking" '+
					' xr-spatial-tracking '+
					' execution-while-out-of-viewport '+
					' execution-while-not-rendered '+
					' web-share '+
					' src="particles/particles.html" >'+
					' </iframe> '+
					' </div> ';


				let cameraFiltersFrame = '<div id="iframeCameraFilters" '+
				  	' class="sketchfab-embed-wrapper"> '+
					' <iframe '+
					' class="sketchfab-embed-wrapper-iframe" '+
					' frameborder="0" '+
					' allowfullscreen '+
					' mozallowfullscreen="true" '+
					' webkitallowfullscreen="true" '+
					' allow="autoplay; fullscreen; xr-spatial-tracking" '+
					' xr-spatial-tracking '+
					' execution-while-out-of-viewport '+
					' execution-while-not-rendered '+
					' web-share '+
					' src="camera/cameraFilters/index.html" >'+
					' </iframe> '+
					' </div> ';


			let jeelizHelmetFrame = '<div id="iframeJeelizHelmet" '+
					' class="sketchfab-embed-wrapper"> '+
				  /*' <iframe '+
				  ' class="sketchfab-embed-wrapper-iframe" '+
				  ' frameborder="0" '+
				  ' allowfullscreen '+
				  ' mozallowfullscreen="true" '+
				  ' webkitallowfullscreen="true" '+
				  ' allow="autoplay; fullscreen; xr-spatial-tracking" '+
				  ' xr-spatial-tracking '+
				  ' execution-while-out-of-viewport '+
				  ' execution-while-not-rendered '+
				  ' web-share '+
				  ' src="https://jeeliz.com/demos/faceFilter/demos/threejs/gltf_fullScreen/" >'+
				  ' </iframe> '+*/
				  ' </div> ';


					

		document.getElementById('frameContainer').innerHTML += particlesFrame;

		document.getElementById('frameContainer').innerHTML += cameraFiltersFrame;

		document.getElementById('frameContainer').innerHTML += jeelizHelmetFrame;

}

function startFlash(code){

    console.log("code: "+code);

	var activate = false;
	let number =  Number(code);
	if(number == 113 || number == 81){
	 params.videoflash = 'on1';
	 activate = true;
	}else if(number == 119|| number == 87){
	 params.videoflash = 'on2';
	 activate = true;
	}else if(number == 101 || number == 69){
	 params.videoflash = 'on3';
	 activate = true;
	}else if(number == 114 || number == 82){
	 params.videoflash = 'on4';
	 activate = true;
	}else if(number == 116 || number == 84){
	 params.videoflash = 'on5';
	 activate = true;
	}else if(number == 121 || number == 89){
	 params.videoflash = 'on6';
	 activate = true;
	}else if(number == 115 || number == 83){
	 params.videoflash = 'off';
	 params.FLASH = '0.0';
	 activate = false;
	 updateParams();
	}
 
	if(activate == true){
	 params.FLASH = '1';
	 //params.BACK = '0.40';
	 updateParams();
	}
   
}

function updateParams(){ 
    let json = JSON.stringify(params); 
    localStorage.setItem('INTERACTIVEVISUALS',json );
}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize(window.innerWidth, window.innerHeight);

	render();

}

//

function opacityIframe(iframe, value){

	document.getElementById(iframe).style.opacity = ''+value;

	if(Number(value) == 0){
		document.getElementById(iframe).style.display = 'none';
	}else if(Number(value) > 0){
		document.getElementById(iframe).style.display = 'block';
	}
}

function changeVideoOpacity(video, value){
	document.getElementById(video).style.opacity = ''+value;
}

function changeVideo(videoName){
	if(currentVideo !== videoName){
		currentVideo = videoName;
		var video = document.getElementById('background-video');
		var src = '../../VIDEO_VISUALS/'+videoName+'.mp4'
		video.setAttribute("src", src);
	}
}
function changeVideoMain(videoName){
	if(currentVideoMain !== videoName){

	
		currentVideoMain = videoName;
		var video = document.getElementById('background-video-main');
		var src = '../../VIDEO_VISUALS/'+videoName+'.mp4'

 

		video.setAttribute("src", src);
	}
}


function changeColor(color){
	if(currentFlashcolor !== color){
		currentFlashcolor = color;
		var flashContainer = document.getElementById('flashContainer');
		flashContainer.style.backgroundColor = color;
	}
}

function changeFlash(videoName){
	if(currentVideo2 !== videoName){
 
		 
		currentVideo2 = videoName;
		var flashContainer = document.getElementById('flashContainer');
		//var src = '../../../VIDEO_VISUALS/'+videoName+'.mp4'
		//video.setAttribute("src", src);

		flashContainer.classList.remove("on1"); 
		flashContainer.classList.remove("on2"); 
		flashContainer.classList.remove("on3"); 
		flashContainer.classList.remove("on4"); 
		flashContainer.classList.remove("on5"); 
		flashContainer.classList.remove("on6"); 
		flashContainer.classList.remove("off");
		flashContainer.classList.add(videoName);
	}
}

function updateTimeLine(setTimeLine, timeLine){
	if(setTimeLine){

		 

		var video = document.getElementById('background-video-main');
		video.currentTime = timeLine;
	
		params.setTimeLine = false;
		let json = JSON.stringify(params); 
		localStorage.setItem('INTERACTIVEVISUALS',json );
	}
}

function syncVideos(syncVideos){
	
	if(syncVideos == true){

		console.log("syncVideos: "+syncVideos);

		var videoBack = document.getElementById('background-video');
		var video = document.getElementById('background-video-main');

		//videoBack.stop();
		//video.stop();

		videoBack.currentTime = 0;
		video.currentTime = 0;

		//videoBack.play();
		//video.play();
 
		params.syncVideos = false;
		let json = JSON.stringify(params); 

		console.log("params.syncVideos: "+params.syncVideos);
		localStorage.setItem('INTERACTIVEVISUALS',json );
	}
}

function render() {

	requestAnimationFrame(render);


	let json = localStorage.getItem('INTERACTIVEVISUALS');

	var jsonObj = JSON.parse(json);
	params = jsonObj;

	 
	opacityIframe('iframe1',params.iframe1);
	opacityIframe('iframe2',params.iframe2);
    opacityIframe('iframe3',params.iframe3);
	opacityIframe('iframe4',params.iframe4);
	opacityIframe('iframe5',params.iframe5);
	opacityIframe('iframe6',params.iframe6);
	opacityIframe('iframe7',params.iframe7);
	opacityIframe('iframe8',params.iframe8);
	opacityIframe('iframe9',params.iframe9); 
	opacityIframe('iframe10',params.iframe10); 
	opacityIframe('iframe11',params.iframe11); 
	opacityIframe('iframe12',params.iframe12);  
	opacityIframe('iframe13',params.iframe13);   
	opacityIframe('iframe14',params.iframe14);   
	//opacityIframe('iframe15',params.iframe15);   
	//opacityIframe('iframe16',params.iframe16);   
	//opacityIframe('iframe17',params.iframe17);   
	//opacityIframe('iframe18',params.iframe18);   
 
	opacityIframe('iframeParticle',params.iframeParticle);
	opacityIframe('iframeCameraFilters',params.iframeCameraFilter);
	//opacityIframe('iframeJeelizHelmet',params.iframeJeelizHelmet);

	changeVideoOpacity('background-video',params.BACK);
	changeVideoOpacity('background-video-main',params.MAIN);
	changeVideoOpacity('flashContainer',params.FLASH);
	
	changeVideo(params.videoBack);
	changeVideoMain(params.videoBackMain);
	changeColor(params.flashColor);
	changeFlash(params.videoflash);

	updateTimeLine(params.setTimeLine,params.timeLine);

	syncVideos(params.syncVideos);
 
}