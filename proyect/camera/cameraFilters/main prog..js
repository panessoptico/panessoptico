
const COL = [
	createCols("https://coolors.co/1f1f1f-525252-7a7a7a-adadad-d6d6d6-ebebeb"),		//Black and white.
	createCols("https://coolors.co/e9ecef-ced4da-adb5bd-6c757d-495057-343a40"),		//White and black.
	createCols("https://coolors.co/003049-d62828-f77f00-fcbf49-eae2b7"),					//Orange.
	createCols("https://coolors.co/000000-1e325c-cb7e01-fda821-fcb84a-fed085-fff7eb"),		//Yellow.
	createCols("https://coolors.co/277da1-4d908e-43aa8b-90be6d-f9c74f-f8961e-f3722c-f85e12-f94144"),	
	createCols("https://coolors.co/0b132b-1c2541-3a506b-5bc0be-6fffe9"),	
	createCols("https://coolors.co/7836a7-8169ce-370d2f-3d37a7-432d90-c545a8-9f3fae-d93d79"),
	createCols("https://coolors.co/0b090a-660708-a4161a-ba181b-e5383b-b1a7a6-d3d3d3"),
	createCols("https://coolors.co/006466-065a60-0b525b-144552-1b3a4b-212f45-272640-312244-3e1f47-4d194d"),	
	createCols("https://coolors.co/006466-065a60-0b525b-144552-1b3a4b-212f45-272640-312244-3e1f47-4d194d-7836a7-8169ce-370d2f-3d37a7-432d90-c545a8-9f3fae-d93d79"),				 

]

let shade;
let Texture;
let colIndex = 0;

function setup() {
	createCanvas(windowWidth, windowHeight, WEBGL);
	frameRate(200);
	noStroke();
	Texture = makeRumpTex(COL[0],false);
	cap = createCapture(VIDEO);
	cap.hide();
	shade = createShader(vert,frag);
	this.shader(shade);
}

function mouseClicked() {
	colIndex++;
	colIndex %= COL.length;
	Texture = makeRumpTex(COL[colIndex],false);
	
}

function draw() {
	background(54);
	
	const ratio = max(width/cap.width,height/cap.height);
	drawRumpedImg(cap,cap.width*ratio,cap.height*ratio,Texture,true);
	
	//const sourceRatio = width*0.15/cap.width;
	//image(cap,(-width+10)/2,(-height+10)/2,cap.width*sourceRatio,cap.height*sourceRatio);
	
	//const texRatio = width*0.15/Texture.width;
	//image(Texture,(-width)/2,(-height+800)/2 + cap.height*sourceRatio,Texture.width*texRatio,Texture.height*texRatio);

}

function windowResized() {
	// Ajusta el tamaño del lienzo al tamaño de la ventana
	resizeCanvas(windowWidth, windowHeight);
  }

function drawRumpedImg(source,wid,hei,rTex,useBlur){
	rectMode(CENTER);
	const imgWid = wid;
	const imgHei = hei;
	shade.setUniform("u_img",source);
	shade.setUniform("u_resolution", [imgWid,imgHei]);
	shade.setUniform("u_Texture",rTex);
	shade.setUniform("u_useBlur", useBlur);
	rect(0,0,imgWid,imgHei);
}

function makeRumpTex(pallet,useLerpcol){
		let gra = createGraphics(200,200);
		gra.noStroke();
		const colNum = pallet.length;
	
		if(!useLerpcol){
			for(let i = 0; i < colNum; i++){
					const x = map(i,0,colNum,0,gra.width);
					gra.fill(pallet[i]);
					gra.rect(x,0,gra.width/colNum,gra.height);
					}
			} 
		else {
					for(let x = 0; x < gra.width; x++){
							const ratio = map(x,0,gra.width,0,1);
							const startColIndex = int(ratio*(colNum-1));
							const endColIndex = int(ratio*(colNum-1))+1;
							const colRatio = (ratio * (colNum-1))%1;
							const col  = lerpColor(color(pallet[startColIndex]),color( pallet[endColIndex]), colRatio);
							gra.fill(col);
							gra.rect(x,0,1,gra.height);
							}
						}
			return gra;
}

function createCols(_url){
  let slash_index = _url.lastIndexOf('/');
  let pallate_str = _url.slice(slash_index + 1);
  let arr = pallate_str.split('-');
  	for (let i = 0; i < arr.length; i++) {
    arr[i] = '#' + arr[i];
 		}
  return arr;
}


