//check off editor's Infinite Loop Protection
//Blur reference: https://www.shadertoy.com/view/XdfGDH

var frag = `
#ifdef GL_ES
precision highp float;
#endif

uniform vec2 u_resolution;
uniform sampler2D u_img;
uniform sampler2D u_Texture;
uniform bool u_useBlur;

varying vec2 var_vertTexCoord;

float grayScale(float r,float g,float b)
{
    float v = 0.30*r + 0.59*g + 0.11*b;
    return v;
}

float normpdf(in float x, in float sigma)
{
	return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
}

vec3 blur(sampler2D img, vec2 uv, vec2 resolution)
{
	vec3 c = texture2D(img, uv).rgb;

	//declare stuff
	const int mSize = 11; 
	const int kSize = (mSize-1)/2;
	float kernel[mSize];
	vec3 final_colour = vec3(0.0);

	//create the 1-D kernel
	float sigma = 7.0;
	float Z = 0.0;

	for (int j = 0; j <= kSize; j++){
		kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), sigma);
	}
	
	//get the normalization factor (as the gaussian has been clamped)
	for (int j = 0; j < mSize; j++){
		Z += kernel[j];
	}

	//read out the texels
	for (int i=-kSize; i <= kSize; i++)
	{
		for (int j=-kSize; j <= kSize; j++){
			final_colour += kernel[kSize+j]*kernel[kSize+i]*texture2D(u_img, (var_vertTexCoord.xy*u_resolution.xy+vec2(float(i),float(j))) / u_resolution.xy).rgb;
		}
	}
	final_colour /= Z*Z;
	return final_colour;
}


void main() {

    //texture
    vec3 tes = u_useBlur == true ? blur(u_img, var_vertTexCoord, u_resolution) : texture2D(u_img,var_vertTexCoord).rgb;
		float tone = grayScale(tes.r,tes.g,tes.b);

		//rump
		vec3 rumpedCol = texture2D(u_Texture,vec2(tone,0.)).rgb;
    
    gl_FragColor = vec4(rumpedCol,1.0);

}
`;

var vert=`
#ifdef GL_ES
	precision highp float;
#endif
#extension GL_OES_standard_derivatives : enable

// attributes, in
attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec2 aTexCoord;
attribute vec4 aVertexColor;

// attributes, out
varying vec3 var_vertPos;
varying vec4 var_vertCol;
varying vec3 var_vertNormal;
varying vec2 var_vertTexCoord;

// matrices
uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;
uniform mat3 uNormalMatrix;

void main() {
	gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(aPosition, 1.0);

	// just passing things through
	var_vertPos = aPosition;
	var_vertCol  = aVertexColor;
	var_vertNormal = aNormal;
	var_vertTexCoord = aTexCoord;
}
`;

